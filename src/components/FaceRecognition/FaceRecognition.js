import React from 'react';
import './FaceRecognition.css';

const FaceRecognition = ({ faceBox, imageUrl }) => {
  return (
    <div className="center ma relative dib">
      <div className="mt2">
        <img id="inputImage" src={imageUrl} alt="" width="500px" height="auto" />
        <div
          className="bounding-box"
          style={{ top: faceBox.topRow, right: faceBox.rightCol, bottom: faceBox.bottomRow, left: faceBox.leftCol }}>
        </div>
      </div>
    </div>
  );
}

export default FaceRecognition;